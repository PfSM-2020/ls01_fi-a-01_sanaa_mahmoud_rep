package de.arktis.Raumschiff;

// Klasse Ladung erstellt
public class Ladung {

     // Private Attribute
     private String bezeichnung;
     private int menge;

     /* voll-parametrisierten Konstruktor
      * @param String bezeichnung - ist die Bezeichnung der Ladung
      * @param int menge - ist die Menge der Ladung
      */
     public Ladung (String bezeichnung, int menge) {
          this.bezeichnung = bezeichnung;
          this.menge = menge;
     }

     // Öffentliche getter -und setter - Methoden

     public String getBezeichnung() {
          /*
          @return bezeichnung - Bezeichnung wird zurückgegeben
           */
          return bezeichnung;
     }

     public void setBezeichnung(String bezeichnung) {
          /*
          @param String bezeichnung - wird übergeben und initialisiert
           */
          this.bezeichnung = bezeichnung;
     }

     public int getMenge() {
          /*
          @return menge - Menge der Ladung wird zurückgeben
           */
          return menge;
     }

     public void setMenge(int menge) {
          /*
          @param int menge - Menge der Ladung wird als Parameter übergeben
           */
          this.menge = menge;
     }
     // Öffentliche toString Methode
     @Override
     public String toString() {
          /*
          @return - alle Werte der gesamten Klasse können in der Konsole ausgegeben werden
           */

          return "Ladung{" +
                  "bezeichnung='" + bezeichnung + '\'' +
                  ", menge=" + menge +
                  '}';
     }
}

