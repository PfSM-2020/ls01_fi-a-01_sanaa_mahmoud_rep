package de.arktis.Raumschiff;

import javax.swing.*;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 Öffentliche Klasse Raumschiff wird angelegt.
 Alle Zustände des Raumschiffes werden auf der Konsole mit entsprechenden Namen ausgeben.
 Es befinden sich darin zwei Listen, welche den Ladungsverzeichnis und den Broadcast Communicator auflistet,
 einen parameterloser Konstruktor und einen vollparametisierten Konstruktor.
 */


public class Raumschiff {

    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;

    // zwei Listen werden hinzugefügt
    private static List<String> broadcastKommunikator = new ArrayList<>();
    private List<Ladung> ladungsverzeichnis = new ArrayList<>();

    // parameterloser Konstruktor
    public Raumschiff() {
    }
    /* vollparametisierter Konstruktor
     * @param photonentorpedoAnzahl gibt die Anzahl der Photonentorpedos aus,
     * @param energieversorgungInProzent zeigt die Energieversorgung in Prozent,
     * @param schildeInProzent gibt die Schildstärke in Prozent aus,
     * @param huelleInProzent gibt die Hüllenstärke in Prozent aus,
     * @param lebenserhaltungssystemeInProzent gibt die Lebenserhaltungssysteme in Prozent aus,
     * @param androidenAnzahl gibt die Anzahl der Androiden aus,
     * @param schiffsname gibt den Namen des Raumschiffes aus

     */

    public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
                      int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
    }
    // getter -und setter- Methoden generiert

     /*
      * @return photonentorpedoAnzahl, wenn die Methode aufgerufen wird, wird die Anzahl
      * der Photonentorpedos ausgegeben
      */

    // @return Anzahl der Photonentopedo wird zurückgegeben
    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        
    }
    // @return Energieversorung wird in Prozent zurückgegeben
    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }
    // @return Stärke der Schilder werden in Prozent zurückgegeben
    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;

    }
    // @return Hüllenstärke wird in Prozent zurückgegeben
    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    // @return Lebenserhaltungssystem wird in Prozent zurückgegeben
    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }
    // @return Androidenanzahl wird zurückgegeben
    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }
    // @return Schiffsname wird zurückgegeben
    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }



    /* Methode ohne Rückgabewert, um das Ladungsverzeichnis zu beladen
     * neue Ladung wird vom Datentyp Ladung in neueLadung hinzugefügt
     */

    public void addLadung(Ladung neueLadung) {
        this.ladungsverzeichnis.add(neueLadung);
    }

    /*
     * Öffentliche Methode ohne Rückgabewert , um Photonentorpedo zu schießen
     */
    public void photonentorpedoSchiessen(Raumschiff r) {
        /* Vorraussetzung: Wenn die Photonentorpedo Anzahl kleiner als 1 ist gilt:
         * eine Nachricht an Alle mit "-=*Click*=-"
         * oder wenn die Photonentorpedo Anzahl größer als 0 ist gilt:
         * eine Nachricht an Alle mit "Photonentorpedo abgeschossen"
         * Treffer werden in r vom Datentyp Raumschiff gespeichert
         */

        if (photonentorpedoAnzahl <1) {
            nachrichtAnAlle("-=*Click*=-");}
        else if(photonentorpedoAnzahl >0) {
            photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            treffer(r);
        }
    }

    // Öffentliche Methode ohne Rückgabewert, um Phaserkanonen zu schießen.

    public void phaserkanoneSchiessen(Raumschiff r) {

        /* Vorraussetzung: Wenn die Energieversorgung weniger als 50% ist gilt:
         * eine Nachricht an Alle mit "-=*Click*=-"
         * oder wenn die photonentorpedoAnzahl Anzahl größer als 50 ist gilt:
         * eine Nachricht an Alle mit "Phasekanone abgeschossen"
         * Treffer werden in r vom Datentyp Raumschiff gespeichert
         */
        if (energieversorgungInProzent < 50) {
            nachrichtAnAlle("-=*Click*=-");}
        else if(photonentorpedoAnzahl > 50) {
            photonentorpedoAnzahl = photonentorpedoAnzahl - 50;
            nachrichtAnAlle("Phaserkanone abgeschossen");
            treffer(r);
        }
    }
    // Private Methode ohne Rückgabewert für Treffer
    private void treffer(Raumschiff r) {
        /*Treffer vermerken:
        * Die Schilder des getroffenen Raumschiffs werden um 50% geschwächt.
        * Sollte anschließend die Schilde vollständig zerstört worden sein, so wird der Zustand der Hülle und der Energieversorgung jeweils um 50% abgebaut.
        * Sollte danach der Zustand der Hülle auf 0% absinken, so sind die Lebenserhaltungssysteme vollständig zerstört und es
        * wird eine Nachricht an Alle ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind.
        *
        *  Schild und Hülle wird um 50% reduziert
        */
        schildeInProzent = schildeInProzent/2; //evtl doch -50
        if (schildeInProzent == 0) {
            huelleInProzent = huelleInProzent/2; //evtl doch -50
            energieversorgungInProzent = energieversorgungInProzent/2; //evtl doch -50
        }
        else {} //else ohne inhalt

        // Konsolenausgabe
        System.out.println(schiffsname + " " + "wurde getroffen!");
        String schiffsname = new String();
    }
    // Öffentliche Methode ohne Rückgabewert, um Nachrichten an alle zu senden
    public void nachrichtAnAlle(String message) {
        broadcastKommunikator.add(message);
        System.out.println(message);

    }

    /* Liste
    *  Logbuch Einträge zurückgeben und gibt den broadcastKommunikator zurück.
     */

    public static List<String> eintraegeLogbuchZurueckgeben() {
        return broadcastKommunikator;
    }

    // öffentliche Methode ohne Rückgabewert, um photonentorpedosLaden in anzahlTorpedos zu laden.
    public void photonentorpedosLaden(int anzahlTorpedos) {
    }

    /*Öffentliche Methode ohne Rückgabewert, um eine Reperatur durchzuführen
    * drei Parameter werden der Methode übergeben:
    * @param boolean schutzschilde - wenn true, dann werden die Schutzschilder neu aufgerüstet
    * @param boolean schiffshuelle - wenn true, dann werden die Schutzschilder neu aufgerüstet
    * @param boolean energieversorgung - wenn true, dann wird die Energieversorgung wieder auf 100% gesetzt
    * @param int anzahlDroiden, wenn weniger als 1, dann werden diese wieder aufgefüllt

     */

    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,boolean schiffshuelle, int anzahlDroiden){
    }

    // Öffentliche Methode ohne Rückgabewert gibt alle Attribute in der Konsole aus
    public void zustandRaumschiff() {
        System.out.println(photonentorpedoAnzahl);
        System.out.println(energieversorgungInProzent);
        System.out.println(schildeInProzent);
        System.out.println(huelleInProzent);
        System.out.println(lebenserhaltungssystemeInProzent);
        System.out.println(androidenAnzahl);
        System.out.println(schiffsname);
        System.out.println(broadcastKommunikator);

    }
    //Öffentliche Methode ohne Rückgabewert gibt das Ladungsverzeichnis aus
    public void ladungsverzeichnisAusgeben() {
        System.out.println(ladungsverzeichnis);

    }
    /*
    * Öffentliche Methode ohne Rückgabewert lässt das Ladungsverzeichnis aufräumen, also
    * Wenn die Menge einer Ladung 0 ist, dann wird das Objekt Ladung aus der Liste entfernt.
    *  1- for each Schleife durch ladungsverzeichnis -> welche Ladung hat Anzahl > 0?
    *  2- die, die größer sind, kommen in eine temporäre Liste
    *  3- ladungsverzeichnis = temporäre liste
     */

    public void ladungsverzeichnisAufraeumen() {

        List<Ladung> tempListe  = new ArrayList<>();
        for ( Ladung ladung:ladungsverzeichnis) {
            if (ladung.getMenge() > 0 ) {
                tempListe.add(ladung);
            }
        }
        ladungsverzeichnis.clear();
        // alle Ladungen in der temporären Liste werden nach dem Löschen der leeren Artikel gespeichert
        ladungsverzeichnis.addAll(tempListe);
    }

    /* Öffentliche toString- Methode, um auf der Konsole Werte auszugeben, welche nicht einfach so klappen
     * @return
     */

    @Override
    public String toString() {
        /*
        @return - alle Werte der Klasse Raumschiff können auf der Konsole ausgegeben werden
         */
        return "Raumschiff{" +
                "photonentorpedoAnzahl=" + photonentorpedoAnzahl +
                ", energieversorgungInProzent=" + energieversorgungInProzent +
                ", schildeInProzent=" + schildeInProzent +
                ", huelleInProzent=" + huelleInProzent +
                ", lebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent +
                ", androidenAnzahl=" + androidenAnzahl +
                ", schiffsname='" + schiffsname + '\'' +
                ", broadcastKommunikator='" + broadcastKommunikator + '\'' +
                ", ladungsverzeichnis=" + ladungsverzeichnis +
                '}';
    }
}
