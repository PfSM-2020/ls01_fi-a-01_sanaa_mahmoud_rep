package de.arktis.Raumschiff;

public class Kapitaen {

     private String name;
     private int entryBeginSince;

     public Kapitaen(String name, int entryBeginSince) {
          this.name = name;
          this.entryBeginSince = entryBeginSince;
     }

     public String getName() {
          return name;
     }

     public void setName(String name) {
          this.name = name;
     }

     public int getEntryBeginSince() {
          return entryBeginSince;
     }

     public void setEntryBeginSince(int entryBeginSince) {
          this.entryBeginSince = entryBeginSince;
     }
}
