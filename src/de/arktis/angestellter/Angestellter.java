package de.arktis.angestellter;

public class Angestellter {

    private String nachname;
    private double gehalt;
    private String vorname;

    //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
    //TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.

    public Angestellter(String nachname, double gehalt, String vorname, String vollname) {
        this.nachname = nachname;
        this.gehalt = gehalt;
        this.vorname = vorname;

    }
    //TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.

    public Angestellter(String nachname, String vorname) {
        this.nachname = nachname;
        this.vorname = vorname;

    }

    public void setNachname(String Nachname) {
        this.nachname = nachname;
    }

    public String getNachname() {
        return this.nachname;
    }

    public void setVorame(String vorname) {
        this.vorname = vorname;
    }

    public String getVorame() {
        return this.vorname;
    }



    public void setGehalt(double gehalt) {
        //TODO: 1. Implementieren Sie die entsprechende set-Methoden.
        //Berücksichtigen Sie, dass das Gehalt nicht negativ sein darf.
        if(gehalt>0) {
            this.gehalt = gehalt;

        }
        else {
            System.out.println("Der Wert muss immer positiv sein.");
        }
    }

    public double getGehalt() {
        //TODO: 2. Implementieren Sie die entsprechende get-Methoden.
        return this.gehalt;
        //return 0.0;
    }

    //TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zurückgibt.
    public String vollname() {
        return vorname + " " + nachname;
    }
}
