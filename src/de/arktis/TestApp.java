package de.arktis;

import de.arktis.Raumschiff.Ladung;
import de.arktis.Raumschiff.Raumschiff;

public class TestApp {

    public static void main(String[] args) {

        Raumschiff enterprise = new Raumschiff(30,70,90,80,3,1,"Enterprise");
        System.out.println(enterprise);
        Ladung kinder = new Ladung("kinder", 65);
        Ladung boerge = new Ladung("boerge", 1);
        enterprise.addLadung(kinder);
        enterprise.addLadung(boerge);
        enterprise.ladungsverzeichnisAusgeben();
        enterprise.ladungsverzeichnisAufraeumen();
        enterprise.ladungsverzeichnisAusgeben();



    }

}
