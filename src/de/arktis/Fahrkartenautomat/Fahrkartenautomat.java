package de.arktis.Fahrkartenautomat;

import java.util.Scanner;		

class Fahrkartenautomat
{
	public static void main(String[] args) 									
	{						
	       LogoBVG();														
	       warte(2000);
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();		
	       double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       fahrkartenAusgeben();											
	       rueckgeldAusgeben(rueckgabebetrag);

	       System.out.println("Vergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	       //Ende des Programmes
	       warte(2000);
	       //Neustart Programm
	       System.out.println("\n\nFahrkartenautomat wird vorbereitet");
	       for (int i = 0; i < 8; i++) 									
	       {
	          System.out.print("=");
	          warte(250);												
	       }
	       
	       System.out.println("\n\n");
	       main(args);												
	       
    }
	public static double fahrkartenbestellungErfassen() 				
	{ 			
		Scanner tastatur = new Scanner(System.in);						
		double preisTicket; 											
		int auswahlFahrkarte;  											
		int anzahlTickets;  											
		double preisGesamt = 0; 										
		double preisGesamtBestellung = 0;								
		int nummerBestellung = 0;										
		String[] arrayBestellungBezeichnung = new String[100];				
		double[] arrayBestellungPreis = new double[100];			
		int[] arrayBestellungAnzahl = new int[100];						
		int bestellungfertig = 0;										
		
			trenner();
			System.out.println("Fahrkartenbestellvorgang");	 	
			trenner();
			System.out.println("Wählen Sie Ihre Wunschfahrkarte aus.          (Nummer eingeben\u2193"); 
			trenner();
			warte(2000);
			System.out.printf("%s%19s%10s%25s%12s\n","\u2193Art","\u2193Tarif","\u2193Zone","\u2193Preis","\u2193Eingabe"); 
			trenner();
			
			//Aufruf der Ticketarten und den zugehörigen Ticketpreisen in Zählschleife
			for (int i = 0; i <=26; i++) 									
				{
				System.out.println(fahrkartenArten(i) + " --> [" + fahrkartenPreis(i) + " EUR] (" + (i + 1) +")"); 
				warte(150);
				}
			trenner();
		while (bestellungfertig == 0)
		{
			//Wahl der Fahrkartenart durch Benutzer
			System.out.println("Bezahlen --> (0)"); 			
			System.out.print("Ihre Wahl: "); 					
			auswahlFahrkarte = tastatur.nextInt();				
		
			//Check ob Bestellung ist fertig (0 = nein, 1 = ja --> Bezahlung Fahrkarten im Warenkorb)
			if (auswahlFahrkarte == 0)
			{
			bestellungfertig = 1; 	
			}
			else					
			{	
				
				//Check Eingabe auf Arraygrenzen
				while ((auswahlFahrkarte < 1) || (auswahlFahrkarte > 27))
				{
				System.out.println(" >>falsche Eingabe<<");		
				trenner();
				System.out.println("Bezahlen --> (0)"); 		
				System.out.print("Ihre Wahl: "); 			
				auswahlFahrkarte = tastatur.nextInt();		
				}
			auswahlFahrkarte -= 1;							
			
			//Anzeige der Auswahl der Fahrkartenart und Fahrkartenpreis
			System.out.print(fahrkartenArten(auswahlFahrkarte));								
			System.out.printf("%s%.2f%5s\n" ," [", fahrkartenPreis(auswahlFahrkarte), "EUR]");	
			System.out.print("Ticketanzahl: ");													
			
			//Ticketanzahl
			anzahlTickets = tastatur.nextInt();									
			anzahlTickets = fahrkartenanzahlKontrolle(anzahlTickets);			
			preisTicket = fahrkartenPreis(auswahlFahrkarte);					
			
			//Anzeige der Auswahl der Fahrkartenanzahl, Fahrkartenart sowie summierten Fahrkartenpreis 
			System.out.print(anzahlTickets + "x " + fahrkartenArten(auswahlFahrkarte)); 						
			System.out.printf("%s%.2f%5s\n" ," [", (fahrkartenPreis(auswahlFahrkarte)  * anzahlTickets), "EUR]");
			
			//Speichern der Fahrkartenanzahl, Fahrkartenart und summierten Fahrkartenpreis in den entsprechenden Arrays zur Ausgabe im Warenkorb
			arrayBestellungBezeichnung[nummerBestellung] =  fahrkartenArten(auswahlFahrkarte); 						
			arrayBestellungPreis[nummerBestellung] =  (fahrkartenPreis(auswahlFahrkarte) * anzahlTickets);	
			arrayBestellungAnzahl[nummerBestellung] = anzahlTickets;										
			preisGesamt = preisTicket * anzahlTickets; 													
			preisGesamtBestellung = preisGesamtBestellung + preisGesamt;  								
			trenner();
			
			//Warenkorb
			System.out.println("Warenkorb:");																
				for (int i = 0; i <= nummerBestellung; i++)													
				{
				System.out.print(arrayBestellungAnzahl[i] + "x " + arrayBestellungBezeichnung[i]);					
				System.out.printf("%s%.2f%5s\n" ," [", arrayBestellungPreis[i], "EUR]");
				}
				
				System.out.printf("%s%4.2f%4s\n", "Summe: " , preisGesamtBestellung , "EUR");
			
				nummerBestellung += 1;															
			trenner();
			}
										
		}
		return preisGesamtBestellung;										
	}
	public static int fahrkartenanzahlKontrolle(int anzT) 
	{			
		Scanner tastatur = new Scanner(System.in);
		while ((anzT > 10) || (anzT < 1)){
			 trenner();
			 System.out.println("Sie haben einen ungültigen Wert als Fahrkartenanzahl eingegeben!");
			 System.out.println("Bitte eine Fahrkartenanzahl zwischen 1 und 10 eingeben.");	
			 trenner();
			 System.out.print("Ticketanzahl: ");
			 anzT = tastatur.nextInt();	
		 }
		return anzT;
	}
	public static String fahrkartenArten(int art)
	{
		String[] arrayFahrkartenarten = new String[27];
		arrayFahrkartenarten[0] = "Kurzstrecke      Regeltarif                    ";
		arrayFahrkartenarten[1] = "Kurzstrecke      ermäßigt                      ";
		arrayFahrkartenarten[2] = "Kurzstrecke      Regeltarif     4-Fahrten-Karte";
		arrayFahrkartenarten[3] = "Kurzstrecke      ermäßigt       4-Fahrten-Karte";
		arrayFahrkartenarten[4] = "Einzelfahrschein Regeltarif AB                 ";
		arrayFahrkartenarten[5] = "Einzelfahrschein Regeltarif BC                 ";
		arrayFahrkartenarten[6] = "Einzelfahrschein Regeltarif ABC                ";
		arrayFahrkartenarten[7] = "Einzelfahrschein ermäßigt   AB                 ";
		arrayFahrkartenarten[8] = "Einzelfahrschein ermäßigt   BC                 ";
		arrayFahrkartenarten[9] = "Einzelfahrschein ermäßigt   ABC                ";
		arrayFahrkartenarten[10] = "Einzelfahrschein Regeltarif AB  4-Fahrten-Karte";
		arrayFahrkartenarten[11] = "Einzelfahrschein Regeltarif BC  4-Fahrten-Karte";
		arrayFahrkartenarten[12] = "Einzelfahrschein Regeltarif ABC 4-Fahrten-Karte";
		arrayFahrkartenarten[13] = "Einzelfahrschein ermäßigt   AB  4-Fahrten-Karte";
		arrayFahrkartenarten[14] = "Einzelfahrschein ermäßigt   BC  4-Fahrten-Karte";
		arrayFahrkartenarten[15] = "Einzelfahrschein ermäßigt   ABC 4-Fahrten-Karte";
		arrayFahrkartenarten[16] = "Tageskarte       Regeltarif AB                 ";
		arrayFahrkartenarten[17] = "Tageskarte       Regeltarif BC                 ";
		arrayFahrkartenarten[18] = "Tageskarte       Regeltarif ABC                ";
		arrayFahrkartenarten[19] = "Tageskarte       ermäßigt   AB                 ";
		arrayFahrkartenarten[20] = "Tageskarte       ermäßigt   BC                 ";
		arrayFahrkartenarten[21] = "Tageskarte       ermäßigt   ABC                ";
		arrayFahrkartenarten[22] = "Kleingruppen-Tageskarte     AB                 ";
		arrayFahrkartenarten[23] = "Kleingruppen-Tageskarte     BC                 ";
		arrayFahrkartenarten[24] = "Kleingruppen-Tageskarte     ABC                ";
		arrayFahrkartenarten[25] = "Anschlussfahrausweis  Regeltarif               ";
		arrayFahrkartenarten[26] = "Anschlussfahrausweis  ermäßigt                 ";
		return arrayFahrkartenarten[art];
	}
	public static double fahrkartenPreis(int preis)
	{
		double[] arrayFahrkartenpreise = new double[27];
		arrayFahrkartenpreise[0] = 1.90;	//KS R
		arrayFahrkartenpreise[1] = 1.40;	//KS e 
		arrayFahrkartenpreise[2] = 5.60;	//KS R     4FK
		arrayFahrkartenpreise[3] = 4.40;	//KS e     4FK
		arrayFahrkartenpreise[4] = 2.90;	//ES R AB
		arrayFahrkartenpreise[5] = 3.30;	//ES R BC
		arrayFahrkartenpreise[6] = 3.60;	//ES R ABC
		arrayFahrkartenpreise[7] = 1.80;	//ES e AB
		arrayFahrkartenpreise[8] = 2.30;	//ES e BC
		arrayFahrkartenpreise[9] = 2.60;	//ES e ABC
		arrayFahrkartenpreise[10] = 9.00;	//ES R AB  4FK
		arrayFahrkartenpreise[11] = 12.00;	//ES R BC  4FK
		arrayFahrkartenpreise[12] = 13.20;	//ES R ABC 4FK
		arrayFahrkartenpreise[13] = 5.60;	//ES e AB  4FK
		arrayFahrkartenpreise[14] = 8.40;	//ES e BC  4FK
		arrayFahrkartenpreise[15] = 9.60;	//ES e ABC 4FK
		arrayFahrkartenpreise[16] = 8.60;	//TK R AB
		arrayFahrkartenpreise[17] = 9.00;	//TK R BC
		arrayFahrkartenpreise[18] = 9.60;	//TK R ABC
		arrayFahrkartenpreise[19] = 5.50;	//TK e AB
		arrayFahrkartenpreise[20] = 5.80;	//TK e BC
		arrayFahrkartenpreise[21] = 6.00;	//TK e ABC
		arrayFahrkartenpreise[22] = 23.50;	//KG e ABC
		arrayFahrkartenpreise[23] = 24.30;	//KG e ABC
		arrayFahrkartenpreise[24] = 24.90;	//KG e ABC
		arrayFahrkartenpreise[25] = 1.70;	//AF R 
		arrayFahrkartenpreise[26] = 1.30;	//AF e 
		return arrayFahrkartenpreise[preis];
	}

	public static double fahrkartenBezahlen(double preisGesamt)
	{ 	
		double bezahltBetrag = 0.0;
		double rueckgabe = 0;
		Scanner tastatur = new Scanner(System.in);
	       while(bezahltBetrag < preisGesamt)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (preisGesamt - bezahltBetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   double bezahlteMuenzen = tastatur.nextDouble();				
	    	   bezahltBetrag += bezahlteMuenzen;						
	    	   rueckgabe = bezahltBetrag - preisGesamt;}
	       return rueckgabe; }

	public static void fahrkartenAusgeben()
	{						
	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          warte(250);												
	       }
	       
	       System.out.println("\n\n");
	}
	public static void warte(int millisekunden) 					
	{					
		 try {
				Thread.sleep(millisekunden);						
			} catch (InterruptedException e) {						
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	public static void rueckgeldAusgeben(double rueckgabe) 
	{			
		if(rueckgabe >= 0.0)
	       {
	    	   System.out.printf("%s%.2f%s", "Der Rueckgabebetrag in Höhe von " , rueckgabe , " EURO");
	    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

	           while(rueckgabe >= 2.0) //
	           {
	        	   muenzeAusgeben(2, " EURO"); // 2 EURO-Muenzen
	        	  rueckgabe -= 2.00;
	           }
	           while(rueckgabe >= 1.0) // 1 EURO-Muenzen
	           {
	        	   muenzeAusgeben(1, " EURO");;
	        	  rueckgabe -= 1.00;
	           }
	           while(rueckgabe >= 0.5) // 50 CENT-Muenzen
	           {
	        	   muenzeAusgeben(50, "CENT");
	        	  rueckgabe -= 0.50;
	           }
	           while(rueckgabe >= 0.20) // 20 CENT-Muenzen
	           {
	        	   muenzeAusgeben(20, "CENT");
	        	  rueckgabe -= 0.20;
	           }
	           while(rueckgabe >= 0.10) // 10 CENT-Muenzen
	           {
	        	   muenzeAusgeben(10, "CENT");
	        	  rueckgabe -= 0.10;
	           }
	           while(rueckgabe >= 0.04)// 5 CENT-Muenzen
	           {
	        	   muenzeAusgeben(5, "CENT");
	        	  rueckgabe -= 0.05;
	           }
	           
	       }
	}
	public static void LogoBVG() 
	{
		System.out.println(
 
				"      _____   _____ \n" + 
				"     | _ ) \\ / / __|\n" + 
				"     | _ \\\\ V / (_ |\n" + 
				"     |___/ \\_/ \\___|\n" + 
				"  #WEIL WIR DICH LIEBEN.\n");
	}
	public static void trenner()
	{
		for (int i = 1; i <= 70; i++) {
			System.out.print("=");
		}
		System.out.println("");
	}
	public static void muenzeAusgeben(int betrag, String einheit)
	{ 
		System.out.printf("%d %s\n", betrag , einheit);
	}
	
	/* Aufgabe 7.1: Vorteile Verwendung ARRAY:
	 * 				Durch die Arrays kann man jedes einzelne Element den Index, direkt ansprechen. 
	 * 				Sonst muesste man jeden einzelnen Wert in einer einzelnen Variable speichern, was sehr aufwändig wäre.
	 * Aufgabe 7.2: Ich hab die Fahrkartenliste noch etwas erweitert.
	 * Aufgabe 7.3: Veränderungen sind einfach und schneller durchzufuehren.
	 *				Außderm lassen sich so auch mehr Fahrkartengruppen und -bezeichnungen implementieren.
	 *				Ich glaube der Zugriff ist auch einfacher und schneller fuer das Prohgramm und es könnte weniger Speicherplatz benötigen
	 */
}